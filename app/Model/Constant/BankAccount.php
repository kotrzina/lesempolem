<?php

namespace Lesempolem\Model\Constant;


interface BankAccount
{

	public const
		ACCOUNT_OWNER = 'SH ČMS - Sbor dobrovolných hasičů Veselice',
		ACCOUNT_NUMBER = '2000928860',
		BANK_CODE = '2010',
		CURRENCY = 'CZK',
		MESSAGE = 'PLATBA STARTOVNEHO NA ZAVODY LESEMPOLEM 2019';

}